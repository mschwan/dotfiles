function! RemoveTrailingWhitespace()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfunction

map <F9> :call RemoveTrailingWhitespace()<CR>
