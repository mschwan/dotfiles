syntax region texZone start="\\begin{lstlisting}" end="\\end{lstlisting}\|%stopzone\>"
syntax region texZone start="\\lstinputlisting" end="{\s*[a-zA-Z/.0-9_^]\+\s*}"
syntax match texInputFile "\\lstinline\s*\(\[.*\]\)\={.\{-}}" contains=texStatement,texInputCurlies,texInputFileOpt
