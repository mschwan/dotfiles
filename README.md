# Dotfiles
These are my personal configuration files plus some handy bash scripts and
LaTeX packages I use.

## Installation
Use the `makefile` to install the needed files. If you want everything to be
installed just type
```
$ make all
```
or
```
$ make config
```
to only install the config files. Use the autocompletion function of your shell
to see all available options.

Do not forget to set your terminal colors according to the [Gruvbox
Theme](https://github.com/morhetz/gruvbox)! If you use gnome-terminal a simple
`make terminal` should set the correct colors.
