.PHONY: texmf config bin terminal

config:
	cp config/bashrc ~/.bashrc
	@if [ -f ~/.config/bash-colors.sh ]; then \
		echo "bash color configuration file exists, skipping"; \
	else \
		cp config/bash-colors.sh ~/.config/bash-colors.sh; \
	fi
	cp config/inputrc ~/.inputrc
	cp config/tmux ~/.tmux.conf
	mkdir -p ~/.config/alacritty/
	cp config/alacritty.toml ~/.config/alacritty/
	mkdir -p ~/.config/fontconfig/
	cp config/fonts.conf ~/.config/fontconfig/
	mkdir -p ~/.vim/swapfiles ~/.vim/undofiles
	cp config/vimrc ~/.vimrc
	cp -r vim/* ~/.vim/
	@if [ -d ~/.vim/bundle/Vundle.vim ]; then \
		echo "Vundle already installed, skipping"; \
	else \
		git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim; \
	fi
	@if command -v pacman > /dev/null; then \
		echo "Installing Archlinux specific configs"; \
		systemctl enable --now --user gcr-ssh-agent.socket; \
		install -Dm 0644 config/makepkg.conf ~/.config/pacman/makepkg.conf; \
	else \
		echo "Not a distro supporting pacman, skipping some Archlinux things"; \
	fi

all: texmf config bin

texmf:
	cp -r texmf/ ~

bin:
	mkdir -p ~/.local/bin/
	cp bin/* ~/.local/bin/
	@echo "Please add ~/.local/bin/ to your PATH!"
